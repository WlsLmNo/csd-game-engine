const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {

  test("hide word", () => {
    expect(GameEngine.hideWord("damn")).toBe("_ _ _ _");
  });

  let context = GameEngine.startGame();
  GameEngine.changeWord(context, "hello");

  test("start a new game with a random word", () => {
    expect(context.display_word.length).toBe(context.word.length * 2 - 1);
  });
  
  test("update the guesses and lives when a wrong guess is given", () => {
    GameEngine.takeGuess(context, "a");
  });

});
