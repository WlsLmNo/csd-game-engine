const random_line = require("@bkbwcm/random-line");

const startGame = () => {
  const context = {
    status: "RUNNING",
    //word: randomWord,
    lives: 5,
    //display_word: hiddenWord,
    guesses: []
  };
  const randomWord = random_line.getRandomWord();
  changeWord(context, randomWord);
  return context;
};

const hideWord = (word) => {
  return ''.padStart(word.length * 2 - 1, "_ ");
}

const changeWord = (context, word) => {
  console.log("=======> word = |" + word + "|");
  const hiddenWord = hideWord(word);
  console.log("=======> hiddenWord = |" + hiddenWord + "|");
  context.word = word;
  context.display_word = hiddenWord;
}

const takeGuess = (game_state, guess) => {
  console.log("========> takeGuess = " + guess)
  return {
    ...game_state,
    lives: game_state.lives - 1
  };
};

module.exports = {
  startGame,
  takeGuess,
  hideWord,
  changeWord
};
